$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var tesimonialsCarousel = $(".testimonials"),
	header = $('header'),
	owl    = $(".owl-carousel"),
	fancy  = $('.fancy');

if(tesimonialsCarousel.length){
  include("js/owl.carousel.js");
}
if($(owl).length){
  include("js/owl.carousel.js");
}

if(fancy.length){
  include("js/jquery.fancybox.pack.js");
}

include("js/jquery.equalheights.js");


function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}

function fixHeader(){
	var bodyScrollTop = $('body').scrollTop(),
		windowH       = $(window).height();

	if ($('body').hasClass('home')){
		if (bodyScrollTop > windowH){
			header.addClass('fixed');
		}
		else{
			header.removeClass('fixed');
		}
	}
}



$(document).ready(function(){

	if (tesimonialsCarousel.length){
		tesimonialsCarousel.owlCarousel({
			items: 2,
			itemsTablet: [767, 1]
		})
	}

	fixHeader();


	$(".accordion").on('click', 'dt', function() {
		var current = $(this),
			targetBox = current.next('dd');

		current.toggleClass('active');
		targetBox.slideToggle();
	})

	if (fancy.length){
		fancy.fancybox();
	}

	$("#nav-icon, #close_icon").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav_overlay');

    })

    /* ------------------------------------------------
	CAROUSEL START
	------------------------------------------------ */

		if(owl.length){
			var sync1 = $("#sync1");
  			var sync2 = $("#sync2");

			sync1.owlCarousel({
				items : 1,
			    singleItem : true,
			    slideSpeed : 1000,
			    navigation: false,
			    pagination:false,
			    afterAction : syncPosition,
			    responsiveRefreshRate : 200,
			});
			 
			sync2.owlCarousel({
			items : 3,
			pagination:false,
			responsiveRefreshRate : 100,
			afterInit : function(el){
			  el.find(".owl-item").eq(0).addClass("synced");
			}
			});
		}

		function syncPosition(el){
				var current = this.currentItem;
				$("#sync2")
				  .find(".owl-item")
				  .removeClass("synced")
				  .eq(current)
				  .addClass("synced")
				if($("#sync2").data("owlCarousel") !== undefined){
				  center(current)
				}
			}
			 
			$("#sync2").on("click", ".owl-item", function(e){
				e.preventDefault();
				var number = $(this).data("owlItem");
				sync1.trigger("owl.goTo",number);
			});
			 
			function center(number){
				var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
				var num = number;
				var found = false;
				for(var i in sync2visible){
				  if(num === sync2visible[i]){
				    var found = true;
				  }
				}

				if(found===false){
				  if(num>sync2visible[sync2visible.length-1]){
				    sync2.trigger("owl.goTo", num - sync2visible.length+2)
				  }else{
				    if(num - 1 === -1){
				      num = 0;
				    }
				    sync2.trigger("owl.goTo", num);
				  }
				} else if(num === sync2visible[sync2visible.length-1]){
				  sync2.trigger("owl.goTo", sync2visible[1])
				} else if(num === sync2visible[0]){
				  sync2.trigger("owl.goTo", num-1)
				}

			}

	/* ------------------------------------------------
	CAROUSEL END
	------------------------------------------------ */

})


$(window).on('scroll', function() {
	fixHeader();
})